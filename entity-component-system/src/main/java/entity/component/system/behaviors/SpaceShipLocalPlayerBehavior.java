package entity.component.system.behaviors;

import entity.component.system.components.UserInputSpaceShipComponent;

public interface SpaceShipLocalPlayerBehavior extends SpaceShipBehavior, CameraBehavior, DirectionalThrustBehavior, UserInputSpaceShipBehavior {}
